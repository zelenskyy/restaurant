### How to run

#### Easy
1. Make sure that docker-compose is installed
2. `./run.sh`

#### Hard
1. package `sbt docker`
2. run `docker run -i com.zelenya/restaurant`
3. run everything else and make them talk to each other...

### How to use

#### Get a given restaurant
`GET http://127.0.0.1:8080/restaurants/<id>`

#### Get the stream of all stored available restaurants (safe)
`GET http://127.0.0.1:8080/restaurants`

#### Get the list of all stored available restaurants (unsafe for big volume of restaurants)
`GET http://127.0.0.1:8080/unsafe/restaurants`

#### Create a restaurant
`POST http://127.0.0.1:8080/restaurants`
```json
{
	"id": 1,
	"name": "Mac",
	"phoneNumber": "+123",
    "cuisinesOffered": ["burger", "cheeseburger"],
    "address": "here",
    "description": "description"
}
```

#### Update a restaurant
`PUT http://127.0.0.1:8080/restaurants`

#### Delete a restaurant
`DELETE http://127.0.0.1:8080/restaurants`

### What 
- the data is stored in PostgresQL, but the storage can be easily replaced
- the solution is built on top of `http4s` and `doobie`
- all stored available restaurants can be obtained by two endpoints, either as streaming or as huge json 
- assumption: the ids are created elsewhere and should not be created on db insert
