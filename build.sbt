val CirceVersion = "0.10.0"
val DoobieVersion = "0.6.0-M3"
val Http4sVersion = "0.19.0-M4"
val LogbackVersion = "1.2.3"
val TestVersion = "3.0.5"

lazy val root = (project in file("."))
  .settings(
    organization := "com.zelenya",
    name := "restaurant",
    version := "0.1",
    scalaVersion := "2.12.7",
    libraryDependencies ++= Seq(
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion, // todo needed?
      "io.circe" %% "circe-generic" % CirceVersion,
      "ch.qos.logback" % "logback-classic" % LogbackVersion,
      "org.tpolecat" %% "doobie-core" % DoobieVersion,
      "org.tpolecat" %% "doobie-postgres" % DoobieVersion,
      "org.scalatest" %% "scalatest" % TestVersion % "test"
    ),
    scalacOptions ++= Seq(
      "-encoding", "UTF-8",
      "-deprecation",
      "-unchecked",
      "-feature",
      "-language:higherKinds",
      "-Xlint",
      "-Xfatal-warnings",
      "-Ypartial-unification"
    ),
    mainClass in assembly := Some("com.zelenya.restaurant.RestaurantApp"),
    dockerfile in docker := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("openjdk:8-jre")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
        expose(8080)
      }
    }
  ).enablePlugins(DockerPlugin)
