addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.7")

addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.5.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.6")
