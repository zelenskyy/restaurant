#!/bin/sh

echo "Packaging, please, wait..."
sbt ";clean; docker"
echo "Running, please, wait..."
docker-compose up
echo "Cleaning, plase, wait..."
docker-compose down
