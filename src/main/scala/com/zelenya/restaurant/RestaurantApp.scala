package com.zelenya.restaurant

import cats.MonadError
import cats.effect.{ExitCode, IO, IOApp}
import cats.syntax.functor._
import com.zelenya.restaurant.http.{Server, SimpleRestaurantService}
import com.zelenya.restaurant.repository.PostgresRestaurantRepository

/**
  * Entry point
  */
object RestaurantApp extends IOApp {
  override def run(args: List[String]): IO[ExitCode] = {
    for {
      repo <- PostgresRestaurantRepository[IO]
      service = new SimpleRestaurantService[IO]()(MonadError[IO, Throwable], repo)
      res <- new Server[IO](service).serve.compile.drain.as(ExitCode.Success)
    } yield res
  }
}
