package com.zelenya.restaurant.entity

case class Restaurant(
  name: String,
  phoneNumber: String,
  cuisinesOffered: List[String],
  address: String,
  description: String
)
