package com.zelenya.restaurant.entity

case class RestaurantWithId(
  id: RestaurantId,
  name: String,
  phoneNumber: String,
  cuisinesOffered: List[String],
  address: String,
  description: String
)

object RestaurantWithId {
  def apply(rId: RestaurantId, r: Restaurant): RestaurantWithId =
    new RestaurantWithId(rId, r.name, r.phoneNumber, r.cuisinesOffered, r.address, r.description)
}

