package com.zelenya.restaurant.entity

sealed trait ValidationError extends Exception
case class RestaurantNotFound(rId: RestaurantId) extends ValidationError
