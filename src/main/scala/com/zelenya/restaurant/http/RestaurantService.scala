package com.zelenya.restaurant.http

import cats.MonadError
import com.zelenya.restaurant.entity._
import cats.implicits._
import com.zelenya.restaurant.repository.RestaurantRepository

trait RestaurantService[F[_]] {
  def getRestaurant(rId: RestaurantId): F[RestaurantWithId]

  def getRestaurants: fs2.Stream[F, RestaurantWithId]

  def createRestaurant(r: RestaurantWithId): F[Unit]

  def updateRestaurant(rId: RestaurantId, r: Restaurant): F[Int]

  def deleteRestaurant(rId: RestaurantId): F[Int]
}

class SimpleRestaurantService[F[_]]()(
  implicit M: MonadError[F, Throwable],
  restaurantRepo: RestaurantRepository[F]
) extends RestaurantService[F] {

  /**
    * Try to find the restaurant by id,
    * return NotFound error otherwise
    */
  def getRestaurant(rId: RestaurantId): F[RestaurantWithId] =
    for {
      restaurant <- restaurantRepo.findRestaurant(rId)
      result <- M.fromOption(restaurant, RestaurantNotFound(rId))
    } yield result

  def getRestaurants: fs2.Stream[F, RestaurantWithId] =
    restaurantRepo.findAll

  def createRestaurant(r: RestaurantWithId): F[Unit] =
    restaurantRepo.insert(r)

  def updateRestaurant(rId: RestaurantId, r: Restaurant): F[Int] =
    restaurantRepo.update(rId, r)

  def deleteRestaurant(rId: RestaurantId): F[Int] =
    restaurantRepo.delete(rId)
}

