package com.zelenya.restaurant.http

import cats.effect.{ConcurrentEffect, ExitCode}
import com.zelenya.restaurant.entity._
import org.http4s.{EntityDecoder, HttpApp, HttpRoutes, InvalidMessageBodyFailure}
import org.http4s.circe.jsonOf
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import cats.implicits._

class Server[F[_] : ConcurrentEffect](service: RestaurantService[F])
  extends Http4sDsl[F] {

  implicit val restaurantDecoder: EntityDecoder[F, Restaurant] = jsonOf[F, Restaurant]
  implicit val restaurantIdDecoder: EntityDecoder[F, RestaurantWithId] = jsonOf[F, RestaurantWithId]

  private val restaurantRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root / "restaurants" / id =>
      service.getRestaurant(id).flatMap(r => Ok(r.asJson))
        .handleErrorWith {
          case RestaurantNotFound(i) => NotFound(i)
        }

    case GET -> Root / "restaurants" =>
      Ok(service.getRestaurants.map(_.asJson))

    case GET -> Root / "unsafe" / "restaurants" =>
      Ok(service.getRestaurants.compile.toList.map(_.asJson))

    case req@POST -> Root / "restaurants" =>
      req.as[RestaurantWithId].flatMap { r =>
        service.createRestaurant(r) *> Created(r.id.asJson)
      }.handleErrorWith {
        case e: InvalidMessageBodyFailure =>
          BadRequest(e.toString.asJson)
      }

    case req@PUT -> Root / "restaurants" / id =>
      req.as[Restaurant].flatMap { r =>
        service.updateRestaurant(id, r).flatMap { n =>
          if (n > 0) NoContent() else NotFound(id)
        }
      }.handleErrorWith {
        case e: InvalidMessageBodyFailure =>
          BadRequest(e.toString.asJson)
      }

    case DELETE -> Root / "restaurants" / id =>
      service.deleteRestaurant(id).flatMap { n =>
        if (n > 0) NoContent() else NotFound(id)
      }

    case GET -> Root / "v1" / "healthcheck" =>
      Ok("42")
  }

  private val httpApp: HttpApp[F] = Router("/" -> restaurantRoutes).orNotFound

  private val serverBuilder: BlazeServerBuilder[F] =
    BlazeServerBuilder[F]
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(httpApp)

  def serve: fs2.Stream[F, ExitCode] = serverBuilder.serve
}
