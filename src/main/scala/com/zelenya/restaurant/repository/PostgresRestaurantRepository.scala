package com.zelenya.restaurant.repository

import cats.MonadError
import cats.effect.{Async, ContextShift}
import cats.implicits._
import com.zelenya.restaurant.entity._
import doobie.implicits._
import doobie.util.query.Query0
import doobie.{ConnectionIO, Transactor, Update}
import doobie.postgres.implicits._

/**
  * Restaurant Repository implementation using PostgresQL
  */
class PostgresRestaurantRepository[F[_]](xa: Transactor[F])
  (implicit F: MonadError[F, Throwable]) extends RestaurantRepository[F] {

  import RestaurantStatement._

  def createDb: F[Int] =
    (drop *> create).transact(xa)

  def findRestaurant(rId: RestaurantId): F[Option[RestaurantWithId]] =
    selectOne(rId).option.transact(xa)

  def findAll: fs2.Stream[F, RestaurantWithId] =
    selectAll.stream.transact(xa)

  def insert(rId: RestaurantWithId): F[Unit] =
    insertQuery.toUpdate0(rId).run.transact(xa) *> F.unit

  def update(rId: RestaurantId, r: Restaurant): F[Int] = {
    val dto = (r.name, r.phoneNumber, r.cuisinesOffered, r.address, r.description, rId)
    updateQuery().toUpdate0(dto).run.transact(xa)
  }

  def delete(rId: RestaurantId): F[Int] =
    deleteQuery(rId).transact(xa)
}

object PostgresRestaurantRepository {

  /**
    * Create postgres repository, create database on start.
    * Simplified and hardcoded
    */
  def apply[F[_] : ContextShift](implicit F: Async[F]): F[PostgresRestaurantRepository[F]] = {
    F.delay(new PostgresRestaurantRepository[F](Transactor.fromDriverManager[F](
      "org.postgresql.Driver", "jdbc:postgresql://db:5432/postgres", "postgres", ""
    ))).flatTap(_.createDb)
  }
}

object RestaurantStatement {
  type RestaurantDTO = (String, String, List[String], String, String, RestaurantId)

  val insertQuery: Update[RestaurantWithId] =
    Update[RestaurantWithId]("INSERT INTO restaurants VALUES (?, ?, ?, ?, ?, ?)", None)

  def updateQuery(): Update[RestaurantDTO] =
    Update[RestaurantDTO](
      s"update restaurants As r set name=?, phoneNumber=?, cuisinesOffered=?, address=?, description=? where r.id=?"
    )

  val drop: ConnectionIO[Int] =
    sql"""
        DROP TABLE IF EXISTS restaurants
      """.update.run

  val create: ConnectionIO[Int] =
    sql"""
        CREATE TABLE restaurants (
          id VARCHAR PRIMARY KEY,
          name VARCHAR NOT NULL,
          phoneNumber VARCHAR NOT NULL,
          cuisinesOffered VARCHAR[] NOT NULL,
          address VARCHAR NOT NULL,
          description VARCHAR NOT NULL
      )
      """.update.run

  val selectAll: Query0[RestaurantWithId] =
    sql"select * from restaurants".query[RestaurantWithId]

  def selectOne(rId: RestaurantId): Query0[RestaurantWithId] =
    sql"select * from restaurants AS r where r.id=$rId".query[RestaurantWithId]

  def deleteQuery(rId: RestaurantId): ConnectionIO[Int] =
    sql"delete from restaurants AS r where r.id=$rId".update.run
}
