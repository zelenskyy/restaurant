package com.zelenya.restaurant.repository

import com.zelenya.restaurant.entity._

/**
  * Repository algebra, allows flexibility and using in-memory implementation in tests
  */
trait RestaurantRepository[F[_]] {
  def findRestaurant(rId: RestaurantId): F[Option[RestaurantWithId]]

  /**
    * Stream restaurants, instead of bringing everything into memory
    */
  def findAll: fs2.Stream[F, RestaurantWithId]

  def insert(r: RestaurantWithId): F[Unit]

  def update(rId: RestaurantId, r: Restaurant): F[Int]

  def delete(rId: RestaurantId): F[Int]
}
