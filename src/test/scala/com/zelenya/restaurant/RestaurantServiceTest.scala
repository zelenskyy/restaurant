package com.zelenya.restaurant

import cats.MonadError
import cats.effect.{ContextShift, IO}
import com.zelenya.restaurant.entity.{Restaurant, RestaurantNotFound, RestaurantWithId}
import com.zelenya.restaurant.http.SimpleRestaurantService
import com.zelenya.restaurant.repository.InMemoryRepository
import org.scalatest.FlatSpec
import cats.implicits._
import com.zelenya.restaurant.RestaurantServiceTest.restaurant1WithId

class RestaurantServiceTest extends FlatSpec {
  import RestaurantServiceTest._

  implicit val cs: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.global)

  val service: IO[SimpleRestaurantService[IO]] = InMemoryRepository.create[IO].map(repo =>
    new SimpleRestaurantService[IO]()(MonadError[IO, Throwable], repo))

  it should "find what was inserted" in {
    val program = service.flatMap { s =>
      s.createRestaurant(restaurant1WithId) *>
        s.getRestaurant(id1)
    }

    assert(program.unsafeRunSync() == restaurant1WithId)
  }

  it should "find everything that was inserted" in {
    val program = service.flatMap { s =>
      s.createRestaurant(restaurant1WithId) *>
        s.createRestaurant(restaurant2WithId) *>
        s.getRestaurants.compile.toList
    }

    val restaurants = program.unsafeRunSync()
    assert(restaurants.contains(restaurant1WithId))
    assert(restaurants.contains(restaurant2WithId))
  }

  it should "find what was updated" in {
    val program = service.flatMap { s =>
      s.createRestaurant(restaurant1WithId) *>
        s.updateRestaurant(id1, restaurant2) *>
        s.getRestaurant(id1)
    }

    assert(program.unsafeRunSync() == RestaurantWithId(id1, restaurant2))
  }

  it should "not find what was deleted" in {
    val program = service.flatMap { s =>
      s.createRestaurant(restaurant1WithId) *>
        s.deleteRestaurant(id1) *>
        s.getRestaurant(id1)
    }

    val result = program.attempt.unsafeRunSync()
    assert(result.isLeft)
    result.fold(
      e => assert(e == RestaurantNotFound(id1)),
      _ => fail()
    )
  }
}

object RestaurantServiceTest {
  val id1 = "1"
  val id2 = "2"

  val restaurant1 = Restaurant(
    "Mac",
    "+123",
    List("burger", "cheeseburger"),
    "here",
    "description"
  )

  val restaurant2 = Restaurant(
    "Another Mac",
    "+123",
    List("burger", "cheeseburger"),
    "there",
    "description"
  )

  val restaurant1WithId = RestaurantWithId(id1, restaurant1)
  val restaurant2WithId = RestaurantWithId(id2, restaurant2)
}
