package com.zelenya.restaurant.repository

import cats.effect.Sync
import cats.effect.concurrent.Ref
import com.zelenya.restaurant.entity.{Restaurant, RestaurantId, RestaurantWithId}
import cats.implicits._
import fs2._

object InMemoryRepository {
  def create[F[_]](implicit F: Sync[F]): F[RestaurantRepository[F]] =
    Ref.of[F, Map[RestaurantId, RestaurantWithId]](Map.empty).map { restaurants =>
      new RestaurantRepository[F] {
        def findRestaurant(rId: RestaurantId): F[Option[RestaurantWithId]] =
          restaurants.get.map(_.get(rId))

        def findAll: Stream[F, RestaurantWithId] =
          Stream.eval(restaurants.get.map(_.values.toList)).flatMap(Stream.emits)

        def insert(r: RestaurantWithId): F[Unit] =
          restaurants.update(_ + ((r.id, r)))

        def update(rId: RestaurantId, r: Restaurant): F[Int] =
          insert(RestaurantWithId(rId, r)) *> F.pure(1)

        def delete(rId: RestaurantId): F[Int] =
          restaurants.update(_ - rId) *> F.pure(1)
      }
    }
}
